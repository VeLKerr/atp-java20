#### Подготовка аннотаций
Из корня проекта компилируем файлы аннотации и процессора.
```bash
javac org/atp/annotation/annotation_processing/BuilderProperty.java
javac org/atp/annotation/annotation_processing/BuilderProcessor.java 
```

#### Запуск кодогенератора
```bash
javac -classpath . -processor org.atp.annotation.annotation_processing.BuilderProcessor org/atp/annotation/Person.java 
```
В classpath добавляем корень проекта (`.`) т.к. не знаем что изначально лежит в classpath.

На выходе должны увидеть скомпилированный класс `Person` и `PersonBuilder`.

#### Доп. матерималы
Библиотека [Java Poet](https://www.baeldung.com/java-poet) для автоматической генерации кода. Облегчает написание методов типа `writeBuilderFile()`.
