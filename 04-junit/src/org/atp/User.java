package org.atp;

import java.util.*;

/**
 * The class for users
 * @since 0.1
 * @author velkerr
 */
public class User {
    private static Map<Integer, User> allUsers = new HashMap<>();
    private static int countId = 0;

    private int id;
    private String name;
    private int age;
    private Sex sex;

    /**
     * Constructor of Users
     * @param name
     * @param age
     * @param sex
     */
    public User(String name, int age, Sex sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;

        if (!hasUser()){
            countId++;
            this.id = countId;
            allUsers.put(id, this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return id == user.id &&
                age == user.age &&
                name.equals(user.name) &&
                sex == user.sex;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, sex);
    }

    /**
     * Convert the user to String
     * @return string representation
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex=" + sex +
                '}';
    }

    private boolean hasUser(){
        for (User user : allUsers.values()){
            if (user.equals(this)){
                return true;
            }
        }
        return false;
    }

    /**
     * @deprecated
     * @return
     */
    public static List<User> getAllUsers(){
        return new ArrayList<>(allUsers.values());
    }

    public static List<User> getAllUsers(Sex sex){
        List<User> listAllUsers = new ArrayList<>();
        for (User user : allUsers.values()){
            if (user.sex == sex){
                listAllUsers.add(user);
            }
        }
        return listAllUsers;
    }

    public static int size(){
        return allUsers.size();
    }

    public static int size(Sex sex){
        return getAllUsers(sex).size();
    }

    public static int getAllAgeUsers(){
        int countAge = 0;
        for (User user : allUsers.values()){
            countAge += user.age;
        }
        return countAge;
    }

    public static int getAllAgeUsers(Sex sex){
        int countAge = 0;
        for (User user : getAllUsers(sex)){
            countAge += user.age;
        }
        return countAge;
    }

    public static float getAverageAgeOfAllUsers(){
        return ((float) getAllAgeUsers()) / size();
    }

    public static float getAverageAgeOfAllUsers(Sex sex){
        return ((float) getAllAgeUsers(sex)) / size(sex);
    }

    protected static void resetDb(){
        allUsers = new HashMap<>();
        countId = 0;
    }
}
