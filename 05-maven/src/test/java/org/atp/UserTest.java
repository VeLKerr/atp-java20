package org.atp;

import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserTest {
    private ArrayList<User> db = new ArrayList<>();

    @BeforeClass
    public static void hello(){
        System.out.println("Testing started");
    }

    @AfterClass
    public static void bye(){
        System.out.println("Testing finished");
    }

    @Before
    public void fillDB(){
        db.add(new User("Евгений", 35, Sex.MALE));
        db.add(new User("Марина", 34, Sex.FEMALE));
        db.add(new User("Алина", 7, Sex.FEMALE));
    }

    @After
    public void clearDB(){
        db.clear();
        User.resetDb();
    }

    @Test
    public void getAllUsers() {
        ArrayList<User> actualUsers = (ArrayList<User>) db.clone();
        actualUsers.add(new User("Евгений1", 35, Sex.MALE));
        actualUsers.add(new User("Марина1", 34, Sex.FEMALE));
        actualUsers.add(new User("Алина1", 7, Sex.FEMALE));

        List<User> expectedUsers = User.getAllUsers();
//        actualUsers.remove(0);
        Assert.assertEquals(expectedUsers, actualUsers);
    }

    @Test
    public void testGetAllUsers() {
        List<User> expectedUsers = User.getAllUsers();
        Assert.assertNotNull(expectedUsers);
    }

    @Test(expected = NullPointerException.class)
    public void testNull(){
        List<User> expectedNull = null;
        expectedNull.add(new User("Алина", 7, Sex.FEMALE));
        Assert.assertNull(expectedNull);
    }

    @Test
    public void size() {
        Assert.fail("FAIL!!");
    }

    @Test
    public void testSize() {
    }

    @Test
    public void getAllAgeUsers() {
    }

    @Test
    public void testGetAllAgeUsers() {
    }

    @Test
    public void getAverageAgeOfAllUsers() {
        float actualsAvg = User.getAverageAgeOfAllUsers();
        float expectedAvg = 25.3f;
        Assert.assertEquals(expectedAvg, actualsAvg, 0.2f);
    }

    @Test
    public void testGetAverageAgeOfAllUsers() {
    }
}