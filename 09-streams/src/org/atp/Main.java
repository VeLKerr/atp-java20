package org.atp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static final String ROOT_DIR = "./griboedov";

    private static void wordCount() throws IOException {
        Stream<Path> innerFiles = Files.walk(Paths.get(ROOT_DIR)).filter(Files::isRegularFile);

        Map<String, Integer> wordcount = Files.lines((Path) innerFiles.toArray()[0]).flatMap(str -> Arrays.stream(str.trim().split("\\s")))
                .map(word -> word.replaceAll("[^a-zA-Z0-9А-Яа-я]", "").toLowerCase())
                .filter(word -> word.length() > 0)
                .map(word -> new SimpleEntry<String, Integer>(word, 1))
//                .collect(Collectors.groupingBy(SimpleEntry::getKey, Collectors.counting()));
                .collect(Collectors.toMap(v -> v.getKey(), v -> v.getValue(), (v1, v2) -> v1 + v2));
        wordcount.forEach((k ,v) -> System.out.println(k + "\t" + v));
    }

    private static void geterateNum(int max){
        ThreadLocalRandom.current().ints(0, max)
                .limit(500)
                .distinct()
                .sorted()
                .forEach(System.out::println);
    }

    public static void main(String[] args) throws IOException {
        Stream.iterate(new long[]{0, 1}, s -> new long[]{s[1], s[0] + s[1]})
                .mapToLong(k -> k[1])
                .peek(System.out::println)
                .limit(100).toArray();
//                .forEach(System.out::println);
    }
}
