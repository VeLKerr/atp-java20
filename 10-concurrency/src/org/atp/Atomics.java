package org.atp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Atomics {
    private static Random random = new Random();

    private static int atomicTest() throws ExecutionException, InterruptedException {
        AtomicInteger ai = new AtomicInteger(0);
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Runnable task = () -> {
            try {
                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                ai.updateAndGet(n->n + 5);
            } catch (InterruptedException e) {
                System.err.println("I'm interrupted :(");
            }
        };

        Future<?> res = null;
        for(int i=0; i<10; i++){
            res = executor.submit(task);
        }
        executor.shutdown();
        res.get();
        return ai.get();
    }

    private static int atomicTest2() throws ExecutionException, InterruptedException {
        AtomicInteger ai = new AtomicInteger(0);
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Runnable task = () -> {
            try {
                TimeUnit.MILLISECONDS.sleep(random.nextInt(500));
                ai.updateAndGet(n->n + 5);
            } catch (InterruptedException e) {
                System.err.println("I'm interrupted :(");
            }
        };

        List<Future<?>> futures = new ArrayList<>();
        for(int i=0; i<10; i++){
            futures.add(executor.submit(task));
        }
        executor.shutdown();
        executor.awaitTermination(500, TimeUnit.MILLISECONDS);
        executor.shutdownNow();
        return ai.get();
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        for(int i=0; i<10; i++){
            System.out.println(atomicTest2());
        }
    }
}
