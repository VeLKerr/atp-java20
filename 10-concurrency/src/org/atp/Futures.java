package org.atp;

import java.util.concurrent.*;

public class Futures {
    public static void main(String[] args) throws ExecutionException, InterruptedException{
        Callable<Integer> task = () -> {
            int sleepTime = 5;
            TimeUnit.SECONDS.sleep(sleepTime);
            return sleepTime;
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> future = executor.submit(task);
        System.out.println("Is done? \n -" + future.isDone());
        int result = 0;
        try {
            result = future.get(1, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            System.err.println("Too early!");
        }
        System.out.println("Is done? \n -" + future.isDone());
        TimeUnit.SECONDS.sleep(3);
        result = future.get();
        System.out.println("The result is: " + result);
        System.out.println("The new result is: " + future.get());
        executor.shutdown();
    }
}
