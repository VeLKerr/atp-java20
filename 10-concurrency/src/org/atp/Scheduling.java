package org.atp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Scheduling {
    private static void simpleScheduling() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> System.out.println("Scheduled at: " + System.currentTimeMillis());
        ScheduledFuture<?> future = executor.schedule(task, 5, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(1);
        long remainingDealy = future.getDelay(TimeUnit.MILLISECONDS);
        System.out.println("Remaining delay: " + remainingDealy);
        executor.shutdown();
    }

    private static void advancedScheduling() throws InterruptedException {
        int executorsLen = 5;
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(executorsLen-2);
        Runnable task = () -> {
            DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss");
            try {
                System.out.println("Scheduled at: " + df.format(LocalDateTime.now()));
                TimeUnit.SECONDS.sleep(5);
                System.out.println("Finished at: " + df.format(LocalDateTime.now()));
            } catch (InterruptedException e) {
                System.err.println("I'm interrupted :(");
            }
        };

        List<ScheduledFuture<?>> futures = new ArrayList<>();
        for(int i=0; i<executorsLen; i++){
            TimeUnit.SECONDS.sleep(1);
//            futures.add(executor.scheduleAtFixedRate(task, 0, 3, TimeUnit.SECONDS));
            futures.add(executor.scheduleWithFixedDelay(task, 0, 3, TimeUnit.SECONDS));
        }
        TimeUnit.SECONDS.sleep(10);
        executor.shutdown();
//        executor.shutdownNow();
    }

    public static void main(String[] args) throws InterruptedException {
        advancedScheduling();
    }
}
