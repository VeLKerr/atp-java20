package org.atp.students;

import java.util.Arrays;

public class Group {
    public String id;
    public Student[] students;

    public Group(String id) {
        this.id = id;
        students = null;
    }

    public Group(String id, Student[] students) {
        this.id = id;
        this.students = students;
    }

    public void addStudent(Student student){
        if (students == null){
            students = new Student[1];
        }
        else {
            students = Arrays.copyOf(students, students.length + 1);
        }
        students[students.length - 1] = student;
    }

    public boolean deleteStudent(Student student){
        for(int i=0; i<students.length; i++){
            if (student.equals(students[i])){
                Student[] newStd = new Student[students.length - 1];
                System.arraycopy(students, 0, newStd, 0, i);
                System.arraycopy(students, i+1, newStd, i, students.length - (i+1));
                students = newStd;
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder a = new StringBuilder("svgedvf");
        a.append("f").append(9).append(0).toString();
        return "Group{" +
                "id='" + id + '\'' +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
