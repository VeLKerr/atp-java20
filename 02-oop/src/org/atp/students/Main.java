package org.atp.students;

public class Main {

    public static void main(String[] args) {
        Group group = new Group("82X");
        group.addStudent(new Student("Alexander", "Grib", "----"));
        group.addStudent(new Student("Oleg", "Ivchenko", null));
        System.out.println(group.toString());
        group.deleteStudent(new Student("Oleg", "Ivchenko"));
        System.out.println(group.toString());
    }
}
