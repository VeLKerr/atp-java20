package org.atp.generics;

public class Wrapper {
    private Object item;

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public static void main(String[] args) {
        Wrapper wrapper = new Wrapper();
        wrapper.setItem(1);
        System.out.println(wrapper
                .getItem().getClass().getSimpleName());
    }
}
