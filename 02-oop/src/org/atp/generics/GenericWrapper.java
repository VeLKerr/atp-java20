package org.atp.generics;

public class GenericWrapper<T> {
    private T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    public static void main(String[] args) {
        GenericWrapper<Integer> wi = new GenericWrapper<>();
        GenericWrapper<String> ws = new GenericWrapper<>();
        wi.setItem(1);

        GenericWrapper ogw = new GenericWrapper();
        ogw.setItem(new Object());
    }
}

