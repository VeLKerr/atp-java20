package org.atp;

public class Main {

    public static void main(String[] args) {
        int i = 123456789;
        float f = i;
        double d = i;
        short s = (short) i;
        System.out.println("int: " + i);
        System.out.println("float: " + f);
        System.out.println("double: " + d);
        System.out.println("short:" + s);

//        int a = 1;
//        int b = 0;
//        int c = a/b;
//        System.out.println(c);

        double ad = 1;
        double bd = 0;
        double cd = ad/bd;
        System.out.println(cd);
        System.out.println(cd + 1);
        System.out.println(0.0 == -0.0);
        System.out.println("a/(+0.0) = " + (ad/(+0.0)));
        System.out.println("a/(-0.0) = " + (ad/(-0.0)));

        Integer num1 = new Integer(120);
        Integer num2 = new Integer(120);
        Integer num3 = 110;
        Integer num4 = 110;
        Integer num5 = 210;
        Integer num6 = 210;

        System.out.println(num1 == num2);
        System.out.println(num3 == num4);
        System.out.println(num5 == num6);
    }
}
