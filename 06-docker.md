0. Ставим Docker
```
curl -sSL https://get.docker.com/ | sh # ставим
for us in $(ls /home/); do usermod -aG docker ${us};
```

или заходим на сервер:
```
ssh jav2020XX@193.200.211.74 -p 1171
```
XX -ваш номер в таблице оценок
пароль: writtenonce

1. Создаем Dockerfile

```
FROM bigdatateam/spark-python2

USER root
WORKDIR /root
```
2. build
```
docker build -t velkerr/my_jupyter .
docker build velkerr/my_jupyter . --no-cache  # не учитываем cache
```

3. run в интерактивном режиме
```
docker run --rm -it velkerr/my_jupyter
```

4. Выполнить команду в контейнере:
```
docker exec -it e183e4c6e3a9 /bin/bash
```

5. Монтиорвание volumes
```
mkdir dir
mkdir readonly
docker run --rm -it -v ${PWD}/dir:/dir -v ${PWD}/readonly:/root/readonly:ro velkerr/my_jupyter
```

6. Проброс портов
Добавляем в Dockerfile `EXPOSE 8888`
```
docker run --rm -it -v ${PWD}/dir:/dir -v ${PWD}/readonly:/root/readonly:ro -p 20000:8888 velkerr/my_jupyter
```
7. Entrypoints - команды, кот. выполняются при запуске (после сборки) контейнера.

8. Итоговый Dockerfile
```
FROM bigdatateam/spark-python2

ENV notebook_user "jovyan"
USER root
WORKDIR /root

RUN apt-get update && apt-get upgrade -y && \
    apt-get install mc htop mlocate tmux python-dev python-pip python3-dev python3-pip -y && \
    updatedb
RUN echo "jovyan ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

ADD entry_point.sh /etc/entry_point.sh
RUN chmod +x /etc/entry_point.sh

EXPOSE 8888
USER ${notebook_user}
WORKDIR /home/${notebook_user}
ENTRYPOINT ["/etc/entry_point.sh"]
```
9. Убить контейнер: `docker kill <container>`.
10. Удалить ненужные контейнеры и образы: `docker system prune`.
