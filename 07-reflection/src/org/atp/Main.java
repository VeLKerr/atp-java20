package org.atp;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    private static void testNames() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new LinkedList<>();
        System.out.println(list1.getClass().getSimpleName() + " " + list2.getClass().getSimpleName());
        Class<? extends List> c1 = list1.getClass();
        List<Integer> list3 = c1.getDeclaredConstructor().newInstance();
        System.out.println(list3.getClass().getSimpleName());
    }

    private static void testClassLoader() throws ClassNotFoundException {
        Class<?> cl = Class.forName("org.atp.Main");
        for(Method m: cl.getDeclaredMethods()){
            System.out.println(m.getName());
        }
    }

    private static void invokeMethod(String methodName) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        Class<?> cl = Class.forName("org.atp.Main");
        for(Method m: cl.getDeclaredMethods()){
            if(m.getName().equals(methodName)){
                m.invoke(new Main());
            }
        }
    }

    public static void changeExternalField() throws ClassNotFoundException, IllegalAccessException {
        Class<?> clazz = Class.forName("org.atp.Other");
        Other other = new Other();
        for(Field f: clazz.getDeclaredFields()){
            f.setAccessible(true);
            System.out.println(f.getInt(other));
            f.set(other, 5);
//            other.field = 5;
            System.out.println(f.getInt(other));
            f.setAccessible(false);
        }
    }

    private static void testLiterals() throws NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<? extends Main> mainCl = Main.class;
        Class<? extends Integer> integerCl = Integer.class;
        Integer exampleInteger = integerCl.getDeclaredConstructor(String.class).newInstance("5");
        System.out.println(integerCl.getCanonicalName());
        Class<? extends Integer> intCl = int.class;
        for(Constructor c: intCl.getConstructors()){
            System.out.println(c.getName());
        }
        Class<? extends int[]> massCl = int[].class;
        System.out.println(massCl.getName());
    }

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, ClassNotFoundException {
//        testNames();
//        testClassLoader();
//        invokeMethod("testClassLoader");
//        changeExternalField();
        testLiterals();
    }
}
