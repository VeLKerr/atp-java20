package org.atp;

import java.util.Set;
import java.util.TreeSet;

public class SetExample {
    public static void main(String[] args) {
        Set<String> set = new TreeSet<>();
        set.add("one");
        set.add("second");
        set.add("3rd");
//        set.add(new Integer(4)); // try uncomment this line
//        set.add(new Float(5.0F)); // try uncomment
        set.add("second"); // duplicate, not added
// set.add(new Integer(4)); // try uncomment
        System.out.println(set);
    }
}
