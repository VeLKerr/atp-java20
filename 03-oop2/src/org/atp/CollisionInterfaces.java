package org.atp;

interface Interface1 {
    void method1(String str);

    default void log(String str){
        System.out.println(this.getClass() + str);
    }
}

interface Interface2 {
    void method2();

    default void log(String str){
        System.out.println(this.getClass() + str);
    }
}

public class CollisionInterfaces implements Interface1, Interface2 {
    @Override
    public void method1(String str) {
        System.out.println("method1");
    }

    @Override
    public void method2() {
        System.out.println("method2");
    }

    @Override
    public void log(String str) {
        ((Interface1)this).log(str);
    }

    public static void main(String[] args) {
        new CollisionInterfaces().log("asdfg");
    }
}
