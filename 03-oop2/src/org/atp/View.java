package org.atp;

import java.util.ArrayList;
import java.util.List;

public class View {
    public static void main(String[]args) {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("e");
        list.add("f");
        System.out.println("list: " + list);
        List view = list.subList(2, 5);
        System.out.println("view: " + view);
        view.remove(1);
        System.out.println("list: " + list);
        System.out.println("view: " + view);
    }
}
