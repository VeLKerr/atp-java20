package org.atp;

interface TestInterface{
    void show();
}

abstract class TestAbstract implements TestInterface{
}

public class TestImpl extends TestAbstract{
    public void show(){}

    public static void main(String[] args) {
        TestImpl ti = new TestImpl();
        ti.show();
    }
}
