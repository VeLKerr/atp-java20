package org.atp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Test {
    private static void fill(Collection<Integer> coll){
        int len = 8;
        for(int i=1; i<len; i++){
            coll.add(i);
        }
    }

    public static void main(String[] args) {
        Collection<Integer> coll = new ArrayList<>();
        fill(coll);
        coll.remove(5);
        System.out.println(coll);

        List<Integer> lst = new ArrayList<>();
        fill(lst);
        lst.remove(5);
        System.out.println(lst);
    }
}
